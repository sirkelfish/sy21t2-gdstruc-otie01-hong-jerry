package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		CardStack stack = new CardStack();
		int input = 2;

		stack.initialPushForTableDeck();

		//With user input version
		do {
			stack.roll();
			stack.printYourStack();
			System.out.println("\nContinue Playing? \nEnter '1' to continue\nEnter '0' to exit ");
			input = scanner.nextInt();
			}	while (input != 0);

		//No user input version
		/*while (input != 0) {
			stack.roll();
			stack.printYourStack();
		}*/
    }
}
