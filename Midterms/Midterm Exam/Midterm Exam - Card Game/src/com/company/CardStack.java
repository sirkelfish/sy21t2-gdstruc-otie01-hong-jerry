package com.company;

import java.util.*;

public class CardStack {
    private int startIndex = 1;
    public int condition = 0;

    public void setCondition(int condition) {
        this.condition = condition;
    }

    Stack<String> tableDeck = new Stack<String>();
    Stack<String> hand = new Stack<String>();
    Stack<String> discardedPile = new Stack<String>();

    //debug purposes
    public void printStax(Stack<String> s) {
        System.out.println(s);
    }

    public static String[] nameList = {"Never", "Gonna", "Give", "You", "Up", "Let", "Down", "Run", "Around", "And", "Desert" };

    public String randomizeName() {
        String randName = nameList[(int) Math.floor(Math.random() * nameList.length)];
        return randName;
    }

    public String getRandName() {
        return randomizeName();
    }

    //pushes a deck of cards then imports it to a new stack
    public void initialPushForTableDeck () {
        Stack<String> curTableDeck = new Stack<String>();
        for (int i = 1; i <= 30; i++) {
            curTableDeck.push(getRandName());
        }
        tableDeck.addAll(curTableDeck); //import
        curTableDeck = null; //clears the old stack
    }

    //prints vertically in reverse as adds to the right each time
    public void reversePrintStack (Stack<String> s) {
        ListIterator<String> rev = s.listIterator(s.size()); //so iterator starts from the end instead
        while (rev.hasPrevious()) {
            System.out.println("Card #" + startIndex + " " + rev.previous());
            startIndex++;
        }
        startIndex = 1;
    }

    //prints the list of cards
    public void printYourStack() {
        System.out.println("\nList of your cards: ");
        reversePrintStack(hand);
    }

    public int rand() {
        Random dice = new Random();
        int commands = dice.nextInt(4 - 1) + 1; // 1-3 only, no 0s
        return commands;
    }

    public int randFive() {
        Random five = new Random();
        int value = five.nextInt(6 - 1) + 1; // 1-5 only, no 0s
        return value;
    }

    public void roll() {
        int tempura = rand();
        int tempR = randFive();

        if(tempura == 1) {
            int tempDOC = tableDeck.size(); //temporarily stores the value of tableDeck into tempDOC
            tempDOC -= tempR; //solves for the sum
            //printStax(hand); Before drawing the cards (For debugging)
            //printStax(tableDeck);

                if (tempDOC > 0) {
                    for (int i = 1; i <= tempR; i++){
                        hand.push(tableDeck.pop()); //tbh I have no idea how this works/worked but hey it works xd
                    }
                    System.out.println("\nYou drew " + tempR + " card(s) from the deck");
                    System.out.println("\nRemaining card(s) on you: " + hand.size());
                    System.out.println("Remaining card(s) on the deck of card(s): " + tableDeck.size());
                    System.out.println("Remaining card(s) on the discarded pile: " + discardedPile.size());
                    setCondition(1);//Condition - you draw any amount of cards
                    //printStax(hand); After drawing the cards (For debugging)
                    //printStax(tableDeck);
                }
                else if (tempDOC == 0) {
                    for (int i = 1; i <= tempR; i++){
                        hand.push(tableDeck.pop());
                    }
                    System.out.println("\nYou drew " + tempR + " card(s) from the deck");
                    System.out.println("\nRemaining card(s) on you: " + hand.size());
                    System.out.println("Remaining card(s) on the deck of card(s): " + tableDeck.size());
                    System.out.println("Remaining card(s) on the discarded pile: " + discardedPile.size());
                }
                else if (tempDOC < 0 )  {
                    System.out.println("\nYou tried to draw " + tempR + " card(s) from the deck but the deck is a little short");
                    System.out.println("No actions made");
                    System.out.println("\nRemaining card(s) on you: " + hand.size());
                    System.out.println("Remaining card(s) on the deck of card(s): " + tableDeck.size());
                    System.out.println("Remaining card(s) on the discarded pile: " + discardedPile.size());
                }
        }
		else if(tempura == 2) {
            int tempHand = hand.size();
            tempHand -= tempR;

            if (tempHand > 0) {
                for (int i = 1; i <= tempR; i++){
                    discardedPile.push(hand.pop());
                }
                System.out.println("\nYou discarded " + tempR + " card(s)");
                System.out.println("\nRemaining card(s) on you: " + hand.size());
                System.out.println("Remaining card(s) on the deck of cards: " + tableDeck.size());
                System.out.println("Remaining card(s) on the discarded pile: " + discardedPile.size());
            }
            else if (tempHand <= 0 && condition > 0){
                //extra condition so it can only happen if you actually drew cards to avoid not being able to start
                System.out.println("\nYou discarded your last " + hand.size() +" card(s) and ran out of cards, You lost.");
                System.out.println("Remaining card(s) on you: 0");
                System.exit(0);
            }
            /*else if (tempHand == 0 && condition == 0 ){
                System.out.println("\nYou tried to discard " + tempR +" card(s) but you don't have any cards");
                System.out.println("Remaining card(s) on you: " + hand.size());
                System.out.println("Remaining card(s) on the deck of cards: " + tableDeck.size());
                System.out.println("Remaining card(s) on the discarded pile: " + discardedPile.size());
            }*///Did not work for some reason D: So I resorted to this v
            else {
                roll();
            }
		}
		else if (tempura == 3) {
            int tempDP = discardedPile.size();
            tempDP -= tempR;

                if (tempDP > 0 && discardedPile.size() != 0) {
                    for (int i = 1; i <= tempR; i++){
                        hand.push(discardedPile.pop());
                    }
                    System.out.println("\nYou drew " + tempR + " card(s) from the discarded pile");
                    System.out.println("\nRemaining card(s) on you: " + hand.size());
                    System.out.println("Remaining card(s) on the deck of cards: " + tableDeck.size());
                    System.out.println("Remaining card(s) on the discarded pile: " + discardedPile.size());
                }
                else if (tempDP == 0) {
                    for (int i = 1; i <= tempR; i++){
                        hand.push(discardedPile.pop());
                    }
                    System.out.println("\nYou drew " + tempR + " card(s) from the discarded pile");
                    System.out.println("\nRemaining card(s) on you: " + hand.size());
                    System.out.println("Remaining card(s) on the deck of cards: " + tableDeck.size());
                    System.out.println("Remaining card(s) on the discarded pile: " + discardedPile.size());
                }
                else if (tempDP < 0 ){
                    System.out.println("\nYou tried to draw " + tempR + " card(s) from the discarded pile but the pile is a little short");
                    System.out.println("No actions made");
                    System.out.println("\nRemaining card(s) on you: " + hand.size());
                    System.out.println("Remaining card(s) on the deck of card(s): " + tableDeck.size());
                    System.out.println("Remaining card(s) on the discarded pile: " + discardedPile.size());
                }
		}
    }
}
