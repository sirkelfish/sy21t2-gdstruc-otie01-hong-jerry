package com.company;

public class Main {

    public static void main(String[] args) {

        int num[] = {7,4,6,55,-5,24,1,5,9,82};

        int numSel[] = {95,1,-8,32,-6,74,203,19,33,4} ;


        System.out.println("Before Bubble Sort: ");
        printArrayElements(num);

        bubbleSort(num);

        System.out.println("\n\nAfter Selection Sort (In Descending Order): ");
        printArrayElements(num);

        System.out.println("\n=========================================\nBefore Selection Sort: ");
        printArrayElements(numSel);

        selectionSort(numSel);

        System.out.println("\n\nAfter Selection Sort (Smallest Number at the end): ");
        printArrayElements(numSel);
    }

    private static void bubbleSort(int[] arr)
    {
        for (int lastSortedIndex = arr.length - 1; lastSortedIndex > 0; lastSortedIndex--)
        {
            for (int i = 0; i < lastSortedIndex; i++)
            {
                if (arr[i] < arr[i + 1])
                {
                    int temp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = temp;
                }
            }
        }
    }

    private static void selectionSort(int[] arr)
    {
        for(int lastSortedIndex = arr.length - 1; lastSortedIndex > 0; lastSortedIndex--)
        {
            int smallestIndex = 0;

            for (int i = 1; i <= lastSortedIndex; i++)
            {
                if (arr[i] < arr[smallestIndex])
                {
                    smallestIndex = i;
                }
            }
            int temp = arr[lastSortedIndex];
            arr[lastSortedIndex] = arr[smallestIndex];
            arr[smallestIndex] = temp;
        }

    }

    private static void printArrayElements(int[] arr)
    {
        for (int j : arr) {
            System.out.print(j + " ");
        }
    }

}
