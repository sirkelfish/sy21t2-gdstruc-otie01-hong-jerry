package com.company;

public class Main {

    public static void main(String[] args) {

        Player asuna = new Player(1,"Asuna", 100);
        Player lethalBacon = new Player(2,"LethalBacon", 205);
        Player hpDeskjet = new Player(3, "HPDeskjet", 34);
        Player honoryKnight = new Player(4,"HonoryKnight", 25);
        Player tunaFish = new Player(5,"TunaFish", 77);

        PlayerLinkedList playerLinkedList = new PlayerLinkedList();

        playerLinkedList.addToFront(asuna);
        playerLinkedList.addToFront(lethalBacon);
        playerLinkedList.addToFront(hpDeskjet);
        playerLinkedList.addToFront(honoryKnight);
        playerLinkedList.removeHead();
        playerLinkedList.addToFront(asuna);
        playerLinkedList.removeHead();

        playerLinkedList.contains(asuna);
        playerLinkedList.contains(tunaFish);
        playerLinkedList.indexOf(lethalBacon);

        playerLinkedList.printList();







    }
}
