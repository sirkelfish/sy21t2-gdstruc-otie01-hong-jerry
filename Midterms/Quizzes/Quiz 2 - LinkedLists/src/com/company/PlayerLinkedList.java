package com.company;

public class PlayerLinkedList {
    private PlayerNode head;
    private PlayerNode tempPointer;
    private static int counter;


    public void addToFront(Player player)
    {
        PlayerNode playerNode = new PlayerNode(player);
        playerNode.setNextPlayer(head);
        head = playerNode;
        incrementCounter();
    }

    public void removeHead()
    {
        tempPointer = head; //points at the current head
        head = head.getNextPlayer(); //switches the head to the next index
        tempPointer.setNextPlayer(null); //removing the former head
        decrementCounter();
    }
    //setter for counter
    public static int getCounter() {
        return counter;
    }

    //adds 1 to counter
    private static void incrementCounter() {
        counter++;
    }

    //minus 1 to counter
    private void decrementCounter() {
        counter--;
    }

    //returns the size via counter
    public int size (){
        return getCounter();
    }

    public boolean contains (Player player) {
        PlayerNode current = head;
        while (current != null) {
            if (current.getPlayer() == player) {
                System.out.println("The element '" + player.getName() + "' Exists!");
                return true;
            }
            current = current.getNextPlayer();
        }
        System.out.println("The element '" + player.getName() + "' Does not exist!");
        return false;
    }

    public int indexOf(Player player) {
        int index = 0;
        PlayerNode current;
        if (player == null) {
            for (current = head; current != null; current = current.getNextPlayer()) {
                if (current.getPlayer() == null) {
                System.out.println("The index of the first found element '" + player.getName() + "' is at index:" + index);
                }
            }
        }
        else {
            for (current = head; current != null; current = current.getNextPlayer()) {
                if (current.getPlayer() == player) {
                System.out.println("The index of the first found element '" + player.getName() + "' is at index:" + index);
                }
                index++;
            }
        }
        return -1;
    }

    public void printList() {
        PlayerNode current = head;
        System.out.print("HEAD -> ");
        while (current != null) {
            System.out.print(current.getPlayer());
            System.out.print(" -> ");
            current = current.getNextPlayer();

        }
        System.out.println("null");
        System.out.println("Size of the Linked List: " + counter);

        }

    }


