package com.company;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<Player> playerList = new ArrayList<>();

        playerList.add(new Player(1,"Asuna",100));
        playerList.add(new Player(2,"LethalBacon",205));
        playerList.add(new Player(3,"HPDeskjet",34));

        //System.out.println(playerList.get(1)); //Access an element

        playerList.add(2, new Player(553,"Arctis", 55)); //Inserting an element (Different[2nd] add function)

        playerList.remove(2); //Removing an Element

        //System.out.println(playerList.contains(new Player(2, "LethalBacon", 205))); //Checks if the element is present
        System.out.println(playerList.indexOf(new Player(2,"LethalBacon", 205))); //Checks for the specific index of the element

        //playerList.forEach(player -> System.out.println(player)); // Simpler version of 'For Each'

        // for (Player p : playerList)
        //{
        //    System.out.println(p);
        //}


    }
}
