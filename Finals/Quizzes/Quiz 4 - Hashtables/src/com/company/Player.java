package com.company;

import java.util.Objects;

public class Player {
    private String UserName;

    public Player(String UserName) {
        this.UserName = UserName;
    }

    public String getUserName() {
        return UserName;
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + UserName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return Objects.equals(UserName, player.UserName);
    }

    @Override
    public int hashCode() {
        return Objects.hash( UserName);
    }
}
