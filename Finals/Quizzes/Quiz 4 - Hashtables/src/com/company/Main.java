package com.company;

public class Main {

    public static void main(String[] args) {

        Player ploo = new Player("Ploo");
        Player wardell = new Player("TSM Wardell");
        Player bomp = new Player("Bomp");
        Player pimp = new Player("Pimp");

        SimpleHashTable hashTable = new SimpleHashTable();
        hashTable.put(ploo.getUserName(), ploo);
        hashTable.put(wardell.getUserName(), wardell);
        hashTable.put(bomp.getUserName(),bomp);

        hashTable.printHashtable();
        System.out.println(hashTable.get("Bomp"));
        hashTable.remove("Ploo");
        hashTable.put(pimp.getUserName(),pimp);
        hashTable.printHashtable();
        System.out.println(hashTable.get("Bomp"));
        hashTable.remove("pog");

    }
}
