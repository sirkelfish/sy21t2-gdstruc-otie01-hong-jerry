package com.company;

public class StoredPlayer {
    public String key;
    public Player value;
    public Player next;
    public Player prev;

    public Player getNext() {
        return next;
    }

    public void setNext(Player next) {
        this.next = next;
    }

    public Player getPrev() {
        return prev;
    }

    public void setPrev(Player prev) {
        this.prev = prev;
    }

    public StoredPlayer(String key, Player value) {
        this.key = key;
        this.value = value;
        next = null;
        prev = null;
    }
}
