package com.company;

import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Scanner;

public class ArrayQueue {

    private static String[] names = { "Jelly", "Flan", "Bottle", "Punk", "Fish", "Dig", "Seven", "Chad", "Rick",
            "Dad", "Spaghetti", "Meatballs", "Cheese"};
    private String[] queuedPlayers;
    private int front;
    private int back;
    int counter = 1;

    public ArrayQueue(int capacity) {
        queuedPlayers = new String[capacity];
    }

    //gets random name from the list
    public String randomizeName() {
        String randName = names[(int) Math.floor(Math.random() * names.length)];
        return randName;
    }

    public String getRandName() {
        return randomizeName();
    }

    public void enqueue(String s) {
        if (back == queuedPlayers.length) {
            String[] newArray = new String[queuedPlayers.length * 2];
            System.arraycopy(queuedPlayers, 0, newArray, 0, queuedPlayers.length);
            queuedPlayers = newArray;
        }

        queuedPlayers[back] = s;
        back++;
    }

    public void rando() {
        int temp = randSeven();
        Scanner readInput = new Scanner(System.in);
        String enterKey;
        System.out.println("Game " + counter + "\n");
        while (counter != 11) {
            //x players enters queue
            for (int i = 1; i <= temp; i++) {
                enqueue("Player " + "- " + getRandName() + " -");
            }

            //if more than or equal to 5, start game
            if (temp >= 5) {
                counter++;

                for (int i = 1; i <= 5; i++) {
                    System.out.println(dequeue() + " has entered the queue.");
                }
                System.out.println("Minimum Players has been met, Game Successfully Started\n");
                System.out.println("Press Enter Key on the next line to Start another Queue.");

                enterKey = readInput.nextLine();

                if(enterKey == "") {
                    System.out.println("In Queue...\n");
                    rando();

                }


            }
            else if (temp < 5) {
                System.out.println("Not enough players to start the game.\n");

                System.out.print("Press Enter Key on the next line to Restart the Queue.\n");

                enterKey = readInput.nextLine();

                if(enterKey == "") {
                    System.out.println("Restarting Queue...\n");
                    rando();
                }
            }
        } if (counter == 11) {
            System.out.println("Oh whoops, I realized that 10 Games has been Successfully made, " +
                    "the program will now terminate. Bye!");
            System.exit(0);
        }
    }

    public String dequeue() {
        //checks if empty (?)
        if (size() == 0) {
            throw new NoSuchElementException();
        }
        String dequeuedPlayer = queuedPlayers[front];
        queuedPlayers[front] = null;
        front++;

        //resets tracker when queue is empty
        if (size() == 0) {
            front = 0;
            back = 0;
        }

        return dequeuedPlayer;
    }

    public String peek() {
        if (size() == 0) {
            throw new NoSuchElementException();
        }
        return queuedPlayers[front];
    }

    public int size() {
        return back - front;
    }

    /*public void printQueue() {
        for (int i = front; i < back; i++) {
            System.out.println(queuedPlayers[i]);
        }
    } */

    public int randSeven() {
        Random seven = new Random();
        int value = seven.nextInt(8 - 1) + 1; // 1-7 only, no 0s
        return value;
    }
}
