package com.company;

public class Node {
    private int data;
    private Node rightChild;
    public Node leftChild;

    public void insert(int value) {
        //duplicate data won't be accepted
        if (value == data) {
            return;
        }

        if (value < data) {
            if (leftChild == null) {
                leftChild = new Node(value);
            } else {
                leftChild.insert(value);
            }
        } else {
            if (rightChild == null) {
                rightChild = new Node(value);
            } else {
                rightChild.insert(value);
            }
        }
    }

    //Ascending Order
    public void traverseInOrder() {

        if (leftChild != null) {
            leftChild.traverseInOrder();
        }

        System.out.println("Data: " + data);

        if (rightChild != null) {
            rightChild.traverseInOrder();
        }

    }

    //Descending Order
    public void traverseInOrderReverse() {
        if (rightChild != null) {
            rightChild.traverseInOrderReverse();
        }

        System.out.println("Data: " + data);

        if (leftChild != null) {
            leftChild.traverseInOrderReverse();
        }
    }

    public Node get(int value) {
        if (value == data) {
            return this;
        }

        if (value < data) {
            if (leftChild != null) {
                return leftChild.get(value);
            }
        }else {
            if (rightChild != null){
                return rightChild.get(value);
            }
        }
        return null;
    }

    public Node getMin(Node x) {

        //checks if there's a root.
        if (x == null) {
            return null;
        }

        Node y = x;

        //find the left most (minimum) child
        while (y.leftChild != null) {
            y = y.leftChild;
        }
        return y;
    }

    public Node getMax(Node x) {

        if (x == null) {
            return null;
        }

        Node y = x;

        while (y.rightChild != null){
            y = y.rightChild;
        }
        return y;
    }

    public Node(int _data) {
        this.data = _data;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public Node getRightChild() {
        return rightChild;
    }

    public void setRightChild(Node rightChild) {
        this.rightChild = rightChild;
    }

    public Node getLeftChild() {
        return leftChild;
    }

    public void setLeftChild(Node leftChild) {
        this.leftChild = leftChild;
    }

    @Override
    public String toString() {
        return "Node{" +
                "data=" + data +
                '}';
    }
}
