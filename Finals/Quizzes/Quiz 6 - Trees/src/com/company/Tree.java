package com.company;

public class Tree {
    public Node root;

    public void insert(int value) {
        if (root == null) {
            root = new Node(value);
        }else {
            root.insert(value);
        }
    }

    public void traverseInOrder() {
        if (root != null) {
            root.traverseInOrder();
        }
    }

    public void traverseInOrderReverse() {
        if (root != null) {
            root.traverseInOrderReverse();
        }
    }

    public Node get(int value) {
        if (root != null)
        {
            return root.get(value);
        }
        return null;
    }

    public Node getMin(Node x) {
        if (root != null)
        {
            return root.getMin(x);
        }
        return null;
    }

    public Node getMax(Node x) {
        if (root != null)
        {
            return root.getMax(x);
        }
        return null;
    }
}
