package com.company;

public class Main {

    public static void main(String[] args) {
        Tree tree = new Tree();

        tree.insert(20);
        tree.insert(3);
        //tree.insert(-90);
        tree.insert(80);
        tree.insert(-3);
        tree.insert(70);
        tree.insert(21);
        tree.insert(4);
        //tree.insert(81);


        System.out.println(tree.root + " Root");
        tree.traverseInOrderReverse();
        System.out.println();
        System.out.println(tree.get(25));
        System.out.println();
        System.out.println(tree.getMin(tree.root));
        System.out.println(tree.getMax(tree.root));
    }
}
